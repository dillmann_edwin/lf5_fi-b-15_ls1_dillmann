
public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.print("Der erste Testsatz.");
		System.out.println("Der Zweite Versuch.");
		
		System.out.print("Der erste Testsatz.");
		System.out.println("Der Zweite \"Versuch!\"");
		
		int alter = 34;
		String name = "Serge Dillmann";
		System.out.print("Mein Name ist " + name + " und ich bin dieses Jahr " + alter + " geworden.");
		// print ist die einfache Text Ausgabe und println die Ausgabe des Textes mit einem Zeilenvorschub
		
		
		System.out.printf("Hello %s!%n", "World");
	
	}

}
