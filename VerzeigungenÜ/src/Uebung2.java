import java.util.Scanner;
public class Uebung2 {

	public static void main(String[] args) {

		System.out.println("Dieser Taschenrechner rechnet mit 2 Zahlen");
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte erste Zahl eingaben ");
		double x = tastatur.nextDouble();
		System.out.println("Bitte zweite Zahl eingaben ");
		double y = tastatur.nextDouble();
		System.out.println("Bitte Rechneoperation eingeben (+,-,*,/)");
		char rechnung = tastatur.next().charAt(0);
		double r = 0;
		
		switch (rechnung) {
		case '+' : r = x + y;
		System.out.println(r);
		break;
		case '-' : r = x - y;
		System.out.println(r);
		break;
		case '*' : r = x * y;
		System.out.println(r);
		break;
		case '/' : r = x / y;
		System.out.println(r);
		break;
		default : System.out.println("Falsche eingabe");
		}
	}

}
