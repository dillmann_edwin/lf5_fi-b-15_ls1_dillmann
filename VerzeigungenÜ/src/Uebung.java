import java.util.Scanner;
public class Uebung {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Dieses Programmen übersetzt Ihre Note in die richtige sprachliche Umschreibung");
		System.out.println("Bitte geben Sie Ihre Note ein ");
		int Note = tastatur.nextInt();
		
		if (Note == 1) {
			System.out.println("Eine Note " + Note + " enstpricht der sprachelichen Umschreibuung von \"Sehr Gut\" ");
		}else if (Note == 2) {
			System.out.println("Eine Note " + Note + " enstpricht der sprachelichen Umschreibuung von \" Gut\" ");
		}else if (Note == 3) {
			System.out.println("Eine Note " + Note + " enstpricht der sprachelichen Umschreibuung von \"Befriedigend\" ");
		}else if (Note == 4 ) {
			System.out.println("Eine Note " + Note + " enstpricht der sprachelichen Umschreibuung von \"Ausreichend\" ");
		}else if (Note == 5) {
			System.out.println("Eine Note " + Note + " enstpricht der sprachelichen Umschreibuung von \"Mangelhaft\" ");
		}else if (Note == 6) {
			System.out.println("Eine Note " + Note + " enstpricht der sprachelichen Umschreibuung von \"Ungenügend\" ");
		}else {
			System.out.println("Bitte eine Note von 1-6 eingeben.");
		}
		

	}

}
