public class AngestellterTest
{
 //------------Hauptprogramm---------------
 public static void main(String[] args)
 {
 Angestellter ang1 = new Angestellter();
 Angestellter ang2 = new Angestellter();
 
 
 //Setzen der Attribute
 ang1.setName("Johanna");
 ang1.setGehalt(50);
 ang2.setName("Edwin");
 ang2.setGehalt(6);
 //Bildschirmausgaben 
 System.out.println("Name: " + ang1.getName());
 System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
 System.out.println("\nName: " + ang2.getName());
 System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
 }//main
}//class