public class MainRaumschiff {

	public static void main(String[] args) {
		
		Raumschiff klingonen = new Raumschiff();
		
		klingonen.setSchiffname("IKS Hegh'ta");
		klingonen.setPhotonentorpedoAnzahl(0);
		klingonen.setAndroidenAnzahl(2);
		klingonen.setEnergieversorgungInProzent(100);
		klingonen.setHuelleInProzent(100);
		klingonen.setLebenserhaltungssystemeInProzent(100);
		klingonen.setSchildeInProzent(100);
			
	}

}
