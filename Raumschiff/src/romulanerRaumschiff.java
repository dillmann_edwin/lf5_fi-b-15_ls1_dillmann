public class romulanerRaumschiff {

	public static void main(String[] args) {
		
		Raumschiff romulaner = new Raumschiff();
		
		romulaner.setSchiffname("IRW Khazara");
		romulaner.setAndroidenAnzahl(2);
		romulaner.setSchildeInProzent(100);
		romulaner.setPhotonentorpedoAnzahl(2);
		romulaner.setEnergieversorgungInProzent(50);
		romulaner.setHuelleInProzent(100);
		romulaner.setLebenserhaltungssystemeInProzent(100);
	}

}