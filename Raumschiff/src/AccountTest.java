public class AccountTest {
	 
	//------------Hauptprogramm---------------
	 public static void main(String[] args)
	 {
	 Account acc1 = new Account();

	 
	 //Setzen der Attribute
	 acc1.setName("Schulz");
	 acc1.setVorname("Marie");
	 acc1.setEmail("Unwichtig@web.de");
	 acc1.setGeschlecht("divers");
	 acc1.setPostleizahl(12679);
	 acc1.setOrt("Bergen");
	 acc1.setPassword("1qayXCV_");
	 acc1.setGeburtsdatum("12.12.2000");
	 acc1.setTelefonnummer(01761234567);
	 acc1.setStrasse("Brückenweg 12");
	
	 //Bildschirmausgaben 
	 System.out.println("Name: " + acc1.getName());
	 System.out.println("Vorname: " + acc1.getVorname());
	 System.out.println("Geburtsdatum: " + acc1.getGeburtsdatum());
	 System.out.println("Tel.: " + acc1.getTelefonnummer());
	 System.out.println("Email: " + acc1.getEmail());
	 System.out.println("Geschlecht: " + acc1.getGeschlecht());
	 System.out.println("Strasse: " + acc1.getStrasse());
	 System.out.print("Ort: " + acc1.getPostleizahl()+ " ");
	 System.out.println(acc1.getOrt());
	 
	 }
}
