import java.util.ArrayList;

public class Raumschiff {
		
		private String schiffname;
		private int androidenAnzahl;
		private int lebenserhaltungssystemeInProzent;
		private int huelleInProzent;
		private int schildeInProzent;
		private int energieversorgungInProzent;
		private int photonentorpedoAnzahl;
		private static ArrayList<String> broadcastKommunikator;
		private ArrayList<Ladung> ladungsverzeichnis;
		
		public Raumschiff()
		{	ladungsverzeichnis = new ArrayList<Ladung>();
			broadcastKommunikator = new ArrayList<String>();	
		}
		
		public Raumschiff(String schiffname, int androidenAnzahl, int lebenserhaltungssystemeInProzent, int huelleInProzent,
		int schildeInProzent, int energieversorgungInProzent, int photonentorpedoAnzahl)
		{	ladungsverzeichnis = new ArrayList<Ladung>();
			broadcastKommunikator = new ArrayList<String>();
			this.schiffname = schiffname;
			this.androidenAnzahl = androidenAnzahl;
			this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
			this.huelleInProzent = huelleInProzent;
			this.schildeInProzent = schildeInProzent;
			this.energieversorgungInProzent = energieversorgungInProzent;
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		}
		
		public void setSchiffname(String schiffname)
		{	this.schiffname = schiffname;	}
		
		public String getSchiffname()
		{	return this.schiffname;  }
		
		public void setAndroidenAnzahl(int androidenAnzahl)
		{	this.androidenAnzahl = androidenAnzahl; }
		
		public int getAndroidenAnzahl()
		{	return this.androidenAnzahl;	}
		
		public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent)
		{	this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;	}
		
		public int getLebenserhaltungssystemeInProzent()
		{	return this.lebenserhaltungssystemeInProzent;	}
		
		public void setHuelleInProzent(int huelleInProzent)
		{	this.huelleInProzent = huelleInProzent;	}
		
		public int getHuelleInProzent()
		{	return this.huelleInProzent;	}
		
		public void setSchildeInProzent(int schildeInProzent)
		{	this.schildeInProzent = schildeInProzent;	}
		
		public int getSchildeInProzent()
		{	return this.schildeInProzent;	}
		
		public void setEnergieversorgungInProzent(int energieversorgungInProzent)
		{	this.energieversorgungInProzent = energieversorgungInProzent;	}
		
		public int getEnergieversorgungInProzent()
		{ return this.energieversorgungInProzent;	}
		
		public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl)
		{	this.photonentorpedoAnzahl = photonentorpedoAnzahl;	}
		
		public int getPhotonentorpedoAnzahl()
		{	return this.photonentorpedoAnzahl;	}
		
		public static ArrayList<String> eintraegeLogbuchZurueckgeben()
		{	return broadcastKommunikator;	}
		
		public ArrayList<Ladung> LadungsverzeichnisZurueckgeben()
		{	return ladungsverzeichnis;	}
		
		public void addLadung(Ladung neueLadung)
		{	ladungsverzeichnis.add(neueLadung);	}
}

