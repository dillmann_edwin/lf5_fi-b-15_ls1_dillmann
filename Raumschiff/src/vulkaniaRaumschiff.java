public class vulkaniaRaumschiff {

	public static void main(String[] args) {
		
		Raumschiff vulkania = new Raumschiff();
		
		vulkania.setSchiffname("NiVar");
		vulkania.setAndroidenAnzahl(5);
		vulkania.setEnergieversorgungInProzent(80);
		vulkania.setHuelleInProzent(50);
		vulkania.setLebenserhaltungssystemeInProzent(100);
		vulkania.setPhotonentorpedoAnzahl(0);
		vulkania.setSchildeInProzent(80);
	}

}
