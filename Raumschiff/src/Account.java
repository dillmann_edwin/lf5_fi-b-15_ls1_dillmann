//Implementierung der Klasse “Angestellter”
public class Account
{
 //Attribute
 private String name;
 private String vorname;
 private String geschlecht;
 private int postleizahl;
 private String ort; 
 private String strasse;
 private long telefonnummer;
 private String email;
 private String geburtsdatum;
 private String password;

 
 
 //Methoden
 
 public void setPassword(String password)
 {
	 this.password = password;
 }
 
 public String getPassword()
 {
	 return this.password;
 }
 
 public void setGeburtsdatum(String geburtsdatum)
 {
	 this.geburtsdatum = geburtsdatum;
 }
 
 public String getGeburtsdatum()
 {
	 return this.geburtsdatum;
 }
 

 public void setEmail(String email)
 {
	 this.email = email;
 }
 
 public String getEmail()
 {
	 return this.email;
 }
 
 public void setTelefonnummer(long telefonnummer)
 {
	 this.telefonnummer = telefonnummer;
 }
 
 public long getTelefonnummer()
 {
	 return this.telefonnummer;
 }
 
 public void setStrasse(String strasse)
 {
	 this.strasse = strasse;
 }
 
 public String getStrasse()
 {
	 return this.strasse;
 }
 
 public void setOrt(String ort)
 {
	 this.ort = ort;
 }
 
 public String getOrt()
 {
	 return this.ort; 
 }
 
 public void setPostleizahl(int postleizahl)
 {
	 this.postleizahl = postleizahl;
 }
 
 public int getPostleizahl()
 {
 return this.postleizahl;
 }
 
 public void setGeschlecht(String geschlecht)
 {
	 this.geschlecht = geschlecht;
 }
 public String getGeschlecht()
 {
 return this.geschlecht;
 }
 
 public void setName(String name)
 {
 this.name = name;
 }
 
 public String getName()
 {
 return this.name;
 }
 
 public void setVorname(String vorname)
 {
 this.vorname = vorname;
 }
 
 public String getVorname()
 {
 return this.vorname;
 }
}